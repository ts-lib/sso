<?php

namespace TsLib\Sso\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

use TsLib\Sso\Models\SSOToken;

class SSOController extends BaseController
{
    //
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function token(Request $request)
    {
        $getToken = SSOToken::where('token', $request->get('t'))->orderBy('created_at', 'DESC')->first();

        if(config('ts-lib-sso.redirect_route_type') == 'name')
        {
            $redirect = redirect()->route(config('ts-lib-sso.redirect_route_value'));
        }
        else
        {
            $redirect = config('ts-lib-sso.redirect_route_value') != null ? redirect(config('ts-lib-sso.redirect_route_value')) : redirect('/');
        }

        if($getToken == null)
        {
            Log::error(sprintf('Se intentó hacer login con una token %s no válida', $request->get('t')));
            return $redirect;
        }

        $date = Carbon::now();
        //Log::info($date->diffInMinutes($getToken->created_at));
        if(\App::environment() == 'production' && $date->diffInMinutes($getToken->created_at) > 3)
        {
            Log::error(sprintf('El usuario %s intentó hacer login con una token %s expirada', $getToken->sales_rep_id, $request->get('t')));
            return $redirect;
        }

        $user_provider = config('auth.providers.users.model');
        $user = $user_provider::find($getToken->sales_rep_id);
        Auth::login($user);

        return $redirect;
    }
}
