<?php

namespace TsLib\Sso;

use Illuminate\Support\ServiceProvider;

class SsoServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->publishes([
		        __DIR__.'/config/sso.php' => config_path('ts-lib-sso.php'),
		    ]);
	}

	public function register()
	{
		$this->loadRoutesFrom(__DIR__.'/routes.php');
	}
}