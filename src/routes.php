<?php

Route::group(['namespace' => 'TsLib\Sso\Controllers'], function()
{
    Route::get('sso', ['middleware' => 'web', 'uses' => 'SSOController@token']);
});