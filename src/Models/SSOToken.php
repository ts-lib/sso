<?php

namespace TsLib\Sso\Models;

use Illuminate\Database\Eloquent\Model;

class SSOToken extends Model
{
    //
    protected $table = "sso_tokens";

    protected $fillable = [
        'token',
        'sales_red_id',
    ];
}
