<?php

return [

	'redirect_route_type' => env('SSO_REDIRECT_ROUTE_TYPE', 'name'),
	'redirect_route_value' => env('SSO_REDIRECT_ROUTE_VALUE', 'home'),

];